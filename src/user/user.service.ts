import {
  Injectable,
  Logger,
  NotFoundException,
  PreconditionFailedException,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import axios from 'axios';
import { IntegrationService } from '../integration/integration.service';
import { CreateUserResponseDto } from './dto/create-user-response.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { Messages } from './messages';
import * as fs from 'fs';

const JWT_ALGORITHM = 'RS256';
const DEFAULT_PASSWORD = '123456';
const DEFAULT_PLATFORM = 'desktop';
const APPROVED_STATUS = 'aprovado';
const USED_STATUS = 'usado';
const DEFAULT_VOUCHER_SOURCE = 'group';
const X_ORG_HEADER = 'X-Org-Id';
const IF_MATCH_HEADER = 'If-Match';
const CONTENT_TYPE_HEADER = 'Content-Type';
const CONTENT_TYPE_HEADER_VALUE = 'application/json';
const PRIVATE_KEY_ENCODE = 'utf-8';

type UpdateTokenResponse = { user: string; token: string; message: string };

@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);

  constructor(
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private readonly integrationService: IntegrationService,
  ) {}

  private readonly auth = {
    username: this.configService.get('MONGO_USER'),
    password: this.configService.get('MONGO_PASS'),
  };

  private readonly voucherApi = axios.create({
    baseURL: this.configService.get('VOUCHER_API'),
    headers: {
      [CONTENT_TYPE_HEADER]: CONTENT_TYPE_HEADER_VALUE,
    },
    auth: this.auth,
  });

  private readonly planApi = axios.create({
    baseURL: this.configService.get('PLAN_API'),
    headers: {
      [CONTENT_TYPE_HEADER]: CONTENT_TYPE_HEADER_VALUE,
    },
    auth: this.auth,
  });

  private readonly userApi = axios.create({
    baseURL: this.configService.get('USER_API'),
    headers: {
      [CONTENT_TYPE_HEADER]: CONTENT_TYPE_HEADER_VALUE,
    },
    auth: this.auth,
  });

  sendDiscordMessage = async (content) => {
    const formatedContent = `---\n\n**API-CONNECT**\n${content}\n\n---`;
    const discordWebhook = this.configService.get<string>('DISCORD_WEBHOOK');
    if (!discordWebhook) {
      this.logger.error(Messages.LOG_ERROR_NOT_FOUND_DISCORD_WEBHOOK);
      return;
    }
    const message = {
      content: formatedContent,
    };
    const headers = {
      'Content-Type': 'application/json',
    };
    return axios
      .post(discordWebhook, message, {
        headers,
      })
      .then(() => {
        this.logger.debug(Messages.LOG_SUCCESS_SEND_DISCORD_MESSAGE);
      })
      .catch((err) => {
        this.logger.error(Messages.LOG_ERROR_SEND_DISCORD_MESSAGE, err);
      });
  };

  private async generateToken({
    email,
    area,
    group,
  }: {
    email: string;
    area: string;
    group: string;
  }): Promise<any> {
    const privateFile = this.configService.get<string>('KEY_FILE');
    const privateKey = fs.readFileSync(privateFile, PRIVATE_KEY_ENCODE);
    if (!privateKey) {
      this.logger.error(Messages.LOG_ERROR_NOT_FOUND_KEY_FILE);
      this.sendDiscordMessage(Messages.LOG_ERROR_NOT_FOUND_KEY_FILE);
      throw new UnprocessableEntityException(Messages.ERROR_ON_GET_KEY_FILE);
    }
    const token = this.jwtService.sign(
      {
        email,
        area,
        group,
        platform: DEFAULT_PLATFORM,
      },
      {
        privateKey,
        algorithm: JWT_ALGORITHM,
      },
    );
    this.logger.debug(Messages.LOG_SUCCESS_TOKEN_GENERATED);
    return token;
  }

  private async registerUser(user: any, group): Promise<any> {
    const headers = {
      [CONTENT_TYPE_HEADER]: CONTENT_TYPE_HEADER_VALUE,
      [X_ORG_HEADER]: group,
    };
    const REGISTER_API = this.configService.get('REGISTER_API');
    return axios
      .post(`${REGISTER_API}/register`, user, {
        headers,
      })
      .then((res) => {
        this.logger.debug(Messages.LOG_SUCCESS_USER_REGISTER);
        return res.data;
      })
      .catch((err) => {
        this.logger.error(Messages.LOG_ERROR_REGISTER_USER_API, err);
        this.sendDiscordMessage(
          `${Messages.LOG_ERROR_REGISTER_USER_API} ${err}`,
        );
        throw new UnprocessableEntityException(Messages.ERROR_ON_CREATE_USER);
      });
  }

  private async getPlan(area: string, email): Promise<any> {
    return this.planApi
      .get(`/plan`, {
        params: {
          where: {
            area,
            user_email: email,
          },
        },
      })
      .then(({ data }) => {
        if (!data?._items?.length) {
          this.logger.error(Messages.LOG_ERROR_NOT_FOUND_PLAN_API);
          this.sendDiscordMessage(Messages.LOG_ERROR_NOT_FOUND_PLAN_API);
          throw new NotFoundException(Messages.ERROR_PLAN_NOT_FOUND);
        }
        this.logger.debug(Messages.LOG_SUCCESS_PLAN_FOUND);
        return data._items[0];
      })
      .catch((err) => {
        this.logger.error(Messages.LOG_ERROR_FIND_PLAN_API, err);
        this.sendDiscordMessage(`${Messages.LOG_ERROR_FIND_PLAN_API} ${err}`);
        throw new PreconditionFailedException(Messages.ERROR_PLAN_NOT_FOUND);
      });
  }

  private async updatePlan({
    id,
    etag,
    group,
    days,
  }: {
    id: string;
    etag: string;
    days: number;
    group: string;
  }): Promise<any> {
    return this.planApi
      .patch(
        `/plan/${id}`,
        {
          remaining_days: days,
        },
        {
          headers: {
            [X_ORG_HEADER]: group,
            [IF_MATCH_HEADER]: etag,
          },
        },
      )
      .then((res) => {
        this.logger.debug(Messages.LOG_SUCCESS_PLAN_UPDATED);
        return res.data;
      })
      .catch((err) => {
        this.logger.error(Messages.LOG_ERROR_UPDATE_PLAN_API, err);
        this.sendDiscordMessage(`${Messages.LOG_ERROR_UPDATE_PLAN_API} ${err}`);
        throw new UnauthorizedException(Messages.ERROR_ON_UPDATE_PLAN);
      });
  }

  private async findAvailableVoucher(group: string): Promise<any> {
    return this.voucherApi
      .get(`/voucher`, {
        params: {
          where: {
            _deleted: false,
            status: APPROVED_STATUS,
            source: DEFAULT_VOUCHER_SOURCE,
            group,
          },
        },
      })
      .then(({ data }) => {
        if (!data?._items?.length) {
          this.logger.error(Messages.LOG_ERROR_NOT_FOUND_VOUCHER_API);
          this.sendDiscordMessage(Messages.LOG_ERROR_NOT_FOUND_VOUCHER_API);
          throw new UnprocessableEntityException(
            Messages.ERROR_VOUCHER_NOT_FOUND,
          );
        }
        this.logger.debug(Messages.LOG_SUCCESS_VOUCHER_FOUND);
        return data._items[0];
      })
      .catch((err) => {
        this.logger.error(Messages.LOG_ERROR_FIND_VOUCHER_API, err);
        this.sendDiscordMessage(
          `${Messages.LOG_ERROR_FIND_VOUCHER_API} ${err}`,
        );
        throw new UnauthorizedException(
          Messages.ERROR_ON_FIND_AVAILABLE_VOUCHER,
        );
      });
  }

  private async updateVouchers(
    voucher: any,
    group: string,
    plan_id: string,
  ): Promise<any> {
    this.voucherApi
      .patch(
        `/voucher/${voucher._id}`,
        { status: USED_STATUS, plan_id, source: DEFAULT_VOUCHER_SOURCE },
        {
          headers: {
            [X_ORG_HEADER]: group,
            [IF_MATCH_HEADER]: voucher._etag,
          },
        },
      )
      .then(() => {
        this.logger.debug(Messages.LOG_SUCCESS_VOUCHER_UPDATED);
        return true;
      })
      .catch((err) => {
        this.logger.error(Messages.LOG_ERROR_UPDATE_VOUCHER_API, err);
        this.sendDiscordMessage(
          `${Messages.LOG_ERROR_UPDATE_VOUCHER_API} ${err}`,
        );
        throw new UnauthorizedException(Messages.ERROR_ON_UPDATE_VOUCHERS);
      });
  }

  private async findUser(email: string): Promise<any> {
    return this.userApi
      .get(`/user/${email}`)
      .then(({ data }) => {
        if (!data) {
          this.logger.error(Messages.LOG_ERROR_NOT_FOUND_USER_API);
          this.sendDiscordMessage(Messages.LOG_ERROR_NOT_FOUND_USER_API);
          throw new NotFoundException(Messages.ERROR_USER_NOT_FOUND);
        }
        this.logger.debug(Messages.LOG_SUCCESS_USER_FOUND);
        return data;
      })
      .catch((err) => {
        this.logger.error(Messages.LOG_ERROR_FIND_USER_API, err);
        this.sendDiscordMessage(`${Messages.LOG_ERROR_FIND_USER_API} ${err}`);
        throw new UnprocessableEntityException(Messages.ERROR_USER_NOT_FOUND);
      });
  }

  async createUser(
    createUserDto: CreateUserDto,
    auth: string,
  ): Promise<CreateUserResponseDto> {
    const { name, email, send_email, days, area } = createUserDto;
    this.logger.debug(Messages.LOG_SUCCESS_INIT_USER_REGISTER, {
      name,
      email,
      send_email,
      days,
      area,
    });

    if (!auth) {
      this.logger.error(Messages.LOG_ERROR_BASIC_AUTH_MISSING);
      this.sendDiscordMessage(Messages.LOG_ERROR_BASIC_AUTH_MISSING);
      throw new UnauthorizedException(Messages.NOT_AUTHORIZED);
    }

    const integration = await this.integrationService.authIntegration({ auth });

    const voucher = await this.findAvailableVoucher(integration.group);

    const token = await this.generateToken({
      email: email,
      area: integration.areas_permissions[0].area,
      group: integration.group,
    });

    const filterArea = integration.areas_permissions.find(
      (areaPermission) => areaPermission.area === area,
    );

    if (!filterArea?.area) {
      this.logger.error(Messages.LOG_ERROR_REQUESTER_AREA_NOT_MATCH);
      this.sendDiscordMessage(Messages.LOG_ERROR_REQUESTER_AREA_NOT_MATCH);
      throw new PreconditionFailedException(Messages.AREA_NOT_FOUND);
    }

    const registerPayload: any = {
      email,
      name,
      surname: '',
      password: DEFAULT_PASSWORD,
      area,
    };

    if (send_email) {
      registerPayload.send_email = 'external';
    }

    await this.registerUser(registerPayload, integration.group);

    const { _id, _etag } = await this.getPlan(filterArea.area, email);

    await this.updateVouchers(voucher, integration.group, _id);

    const limitedDays =
      days && days <= voucher.remaining_days ? days : voucher.remaining_days;

    await this.updatePlan({
      id: _id,
      etag: _etag,
      days: limitedDays,
      group: integration.group,
    });

    this.sendDiscordMessage({
      email,
      area,
      group: integration.group,
      description: Messages.DISCORD_MESSAGE_USER_REGISTER_BY_INTEGRATION,
    });

    return {
      user: registerPayload.email,
      token,
      message: Messages.USER_CREATED_SUCCESSFULLY,
    };
  }

  async reactivateUser(days, email, area, auth): Promise<any> {
    this.logger.debug(Messages.LOG_SUCCESS_INIT_USER_REACTIVATION, {
      days,
      email,
      area,
    });
    if (!auth) {
      this.logger.error(Messages.LOG_ERROR_BASIC_AUTH_MISSING);
      this.sendDiscordMessage(Messages.LOG_ERROR_BASIC_AUTH_MISSING);
      throw new UnauthorizedException(Messages.NOT_AUTHORIZED);
    }
    const integration = await this.integrationService.authIntegration({ auth });

    const voucher = await this.findAvailableVoucher(integration.group);

    const filterArea = integration.areas_permissions.find(
      (areaPermission) => areaPermission.area === area,
    );

    if (!filterArea?.area) {
      this.logger.error(Messages.LOG_ERROR_REQUESTER_AREA_NOT_MATCH);
      this.sendDiscordMessage(Messages.LOG_ERROR_REQUESTER_AREA_NOT_MATCH);
      throw new PreconditionFailedException(Messages.AREA_NOT_FOUND);
    }

    const { _id, _etag } = await this.getPlan(filterArea.area, email);

    await this.updateVouchers(voucher, integration.group, _id);

    const limitedDays =
      days && days <= voucher.remaining_days ? days : voucher.remaining_days;

    await this.updatePlan({
      id: _id,
      etag: _etag,
      days: limitedDays,
      group: integration.group,
    });

    return {
      email,
      message: Messages.USER_UPDATED_SUCCESSFULLY,
    };
  }

  async disableUser(email, area, auth): Promise<any> {
    this.logger.debug(Messages.LOG_SUCCESS_INIT_USER_DEACTIVATION, {
      email,
      area,
    });
    if (!auth) {
      this.logger.error(Messages.LOG_ERROR_BASIC_AUTH_MISSING);
      this.sendDiscordMessage(Messages.LOG_ERROR_BASIC_AUTH_MISSING);
      throw new UnauthorizedException(Messages.NOT_AUTHORIZED);
    }
    const integration = await this.integrationService.authIntegration({ auth });

    await this.findUser(email);

    const filterArea = integration.areas_permissions.find(
      (areaPermission) => areaPermission.area === area,
    );

    if (!filterArea?.area) {
      this.logger.error(Messages.LOG_ERROR_REQUESTER_AREA_NOT_MATCH);
      this.sendDiscordMessage(Messages.LOG_ERROR_REQUESTER_AREA_NOT_MATCH);
      throw new PreconditionFailedException(Messages.AREA_NOT_FOUND);
    }

    const { _id, _etag } = await this.getPlan(filterArea.area, email);

    await this.updatePlan({
      id: _id,
      etag: _etag,
      days: 0,
      group: integration.group,
    });

    return {
      email,
      message: Messages.USER_DISABLED_SUCCESSFULLY,
    };
  }

  async updateToken(email, area, auth): Promise<UpdateTokenResponse> {
    this.logger.debug(Messages.LOG_SUCCESS_INIT_USER_TOKEN_UPDATE, {
      email,
      area,
    });
    if (!auth) {
      this.logger.error(Messages.LOG_ERROR_BASIC_AUTH_MISSING);
      this.sendDiscordMessage(Messages.LOG_ERROR_BASIC_AUTH_MISSING);
      throw new UnauthorizedException(Messages.NOT_AUTHORIZED);
    }
    const integration = await this.integrationService.authIntegration({ auth });

    await this.findUser(email);

    const filterArea = integration.areas_permissions.find(
      (areaPermission) => areaPermission.area === area,
    );

    if (!filterArea?.area) {
      this.logger.error(Messages.LOG_ERROR_REQUESTER_AREA_NOT_MATCH);
      this.sendDiscordMessage(Messages.LOG_ERROR_REQUESTER_AREA_NOT_MATCH);
      throw new PreconditionFailedException(Messages.AREA_NOT_FOUND);
    }

    const token = await this.generateToken({
      email,
      area: filterArea.area,
      group: integration.group,
    });

    return {
      user: email,
      token,
      message: Messages.TOKEN_UPDATED_SUCCESSFULLY,
    };
  }
}
