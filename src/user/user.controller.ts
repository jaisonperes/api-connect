import { Body, Controller, Delete, Patch, Post, Request } from '@nestjs/common';
import {
  ApiBasicAuth,
  ApiBody,
  ApiNotFoundResponse,
  ApiPreconditionFailedResponse,
  ApiResponse,
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { CreateUserResponseDto } from './dto/create-user-response.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { DisableUserDto } from './dto/disable-user.dto';
import { ReactivateUserDto } from './dto/reactivate-user.dto';
import { UpdateUserTokenDto } from './dto/update-user-token.dto';
import { Messages } from './messages';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @ApiBody({ type: CreateUserDto })
  @ApiResponse({
    status: 201,
    description: Messages.CREATED,
  })
  @ApiUnauthorizedResponse({
    description: Messages.NOT_AUTHORIZED_EXCEPTION,
  })
  @ApiUnprocessableEntityResponse({
    description: Messages.CREATE_UNPROCESSABLE_ENTITY,
  })
  @ApiBasicAuth()
  async createUser(
    @Body() createUserDto: CreateUserDto,
    @Request() req,
  ): Promise<CreateUserResponseDto> {
    const { authorization } = req.headers;
    return await this.userService.createUser(createUserDto, authorization);
  }

  @Post('/token')
  @ApiBody({ type: UpdateUserTokenDto })
  @ApiResponse({
    status: 201,
    description: Messages.TOKEN_UPDATED,
  })
  @ApiUnauthorizedResponse({
    description: Messages.NOT_AUTHORIZED_EXCEPTION,
  })
  @ApiUnprocessableEntityResponse({
    description: Messages.UPDATE_NOT_FOUND_EXCEPTION,
  })
  @ApiPreconditionFailedResponse({
    description: Messages.UPDATE_PRECONDITION_EXCEPTION,
  })
  @ApiBasicAuth()
  async updateToken(
    @Body() updateUserTokenDto: UpdateUserTokenDto,
    @Request() req,
  ): Promise<any> {
    const { authorization } = req.headers;
    const { email, area } = updateUserTokenDto;
    return await this.userService.updateToken(email, area, authorization);
  }

  @Patch()
  @ApiBody({ type: ReactivateUserDto })
  @ApiResponse({
    status: 201,
    description: Messages.UPDATED,
  })
  @ApiUnauthorizedResponse({
    description: Messages.NOT_AUTHORIZED_EXCEPTION,
  })
  @ApiNotFoundResponse({
    description: Messages.UPDATE_NOT_FOUND_EXCEPTION,
  })
  @ApiPreconditionFailedResponse({
    description: Messages.PATCH_PRECONDITION_EXCEPTION,
  })
  @ApiBasicAuth()
  async reactivateUser(
    @Body() reactivateUserDto: ReactivateUserDto,
    @Request() req,
  ): Promise<CreateUserResponseDto> {
    const { authorization } = req.headers;
    const { days, area, email } = reactivateUserDto;
    return await this.userService.reactivateUser(
      days,
      email,
      area,
      authorization,
    );
  }

  @Delete()
  @ApiBody({ type: DisableUserDto })
  @ApiResponse({
    status: 201,
    description: Messages.DISABLED,
  })
  @ApiUnauthorizedResponse({
    description: Messages.NOT_AUTHORIZED_EXCEPTION,
  })
  @ApiNotFoundResponse({
    description: Messages.UPDATE_NOT_FOUND_EXCEPTION,
  })
  @ApiBasicAuth()
  async disableUser(
    @Body() disableUserDto: DisableUserDto,
    @Request() req,
  ): Promise<void> {
    const { authorization } = req.headers;
    const { area, email } = disableUserDto;
    return await this.userService.disableUser(email, area, authorization);
  }
}
