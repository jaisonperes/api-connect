export class CreateUserResponseDto {
  user: string;
  token: string;
  message: string;
}
