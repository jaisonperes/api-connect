import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  Matches,
  MinLength,
} from 'class-validator';
import { Messages } from '../messages';

const CPF_REGEX_WHTHOUT_DASH = /^\d{11}$/;

export class CreateUserDto {
  @IsString({ message: Messages.NAME_FIELD_IS_REQUIRE })
  @MinLength(3, {
    message: Messages.NAME_FIELD_IS_TOO_SHORT,
  })
  @ApiProperty({
    type: String,
    description: 'New user name',
    example: 'John Doe',
  })
  name: string;

  @Matches(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i, {
    message: Messages.EMAIL_NOT_VALID,
  })
  @ApiProperty({
    type: String,
    description: 'New user email',
    example: 'johndoe@mail.com',
  })
  email: string;

  @IsOptional()
  @IsNumber({}, { message: Messages.NOT_NUMBER })
  @ApiProperty({
    type: Number,
    description: 'Days of plan to be active',
    example: 30,
    required: false,
  })
  days: number;

  @IsString({
    message: Messages.AREA_FIELD_IS_REQUIRE,
  })
  @ApiProperty({
    type: String,
    description: 'New user area',
    example: 'areaname',
  })
  area: string;

  @IsOptional()
  @Matches(CPF_REGEX_WHTHOUT_DASH, {
    message: Messages.CPF_IS_NOT_VALID,
  })
  @ApiProperty({
    type: String,
    description: 'New user cpf',
    example: '12345678901',
    required: false,
  })
  cpf: string;

  @IsBoolean({
    message: Messages.SEND_EMAIL_IS_REQUIRE,
  })
  @ApiProperty({
    type: Boolean,
    description: 'Send email to new user',
    example: true,
  })
  send_email: boolean;
}
