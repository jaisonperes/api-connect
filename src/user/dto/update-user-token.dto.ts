import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString, Matches } from 'class-validator';
import { Messages } from '../messages';

export class UpdateUserTokenDto {
  @Matches(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i, {
    message: Messages.EMAIL_NOT_VALID,
  })
  @ApiProperty({
    type: String,
    description: 'Email of the user',
    example: 'johndoe@mail.com',
  })
  email: string;

  @IsString({
    message: Messages.AREA_FIELD_IS_REQUIRE,
  })
  @ApiProperty({
    type: String,
    description: 'Area of the user',
    example: 'areaname',
  })
  area: string;
}
