import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString, Matches } from 'class-validator';
import { Messages } from '../messages';

export class ReactivateUserDto {
  @Matches(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i, {
    message: Messages.EMAIL_NOT_VALID,
  })
  @ApiProperty({
    type: String,
    description: 'Email to be reactivated',
    example: 'johndoe@mail.com',
  })
  email: string;

  @IsOptional()
  @IsNumber({}, { message: Messages.NOT_NUMBER })
  @ApiProperty({
    type: Number,
    description: 'Days of plan to be activated',
    example: 30,
    required: false,
  })
  days: number;

  @IsString({
    message: Messages.AREA_FIELD_IS_REQUIRE,
  })
  @ApiProperty({
    type: String,
    description: 'Area of the user',
    example: 'areaname',
  })
  area: string;
}
