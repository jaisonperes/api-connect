import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import * as fs from 'fs';
import { IntegrationModule } from 'src/integration/integration.module';
import { IntegrationService } from 'src/integration/integration.service';
import { IntegrationSchema } from 'src/integration/integration.model';
import { MongooseModule } from '@nestjs/mongoose';

const INTEGRATION_SCHEMA_NAME = 'Integration';
const JWT_ALGORITHM = 'RS256';
const PRIVATE_KEY_ENCODE = 'utf-8';

@Module({
  imports: [
    ConfigModule,
    IntegrationModule,
    MongooseModule.forFeature([
      {
        name: INTEGRATION_SCHEMA_NAME,
        schema: IntegrationSchema,
      },
    ]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        const privateFile = configService.get<string>('JWT_KEY_FILE');
        const privatekey = fs.readFileSync(privateFile, PRIVATE_KEY_ENCODE);
        const options: JwtModuleOptions = {
          privateKey: privatekey,
          signOptions: {
            expiresIn: configService.get<string>('JWT_ALGORITHM'),
            issuer: configService.get<string>('JWT_EXPIRES'),
            algorithm: JWT_ALGORITHM,
          },
        };
        return options;
      },
      inject: [ConfigService],
    }),
  ],
  controllers: [UserController],
  providers: [UserService, ConfigService, IntegrationService],
})
export class UserModule {}
