import { Schema } from 'mongoose';

const INTEGRATION_COLLECTION = 'integration';

export const IntegrationSchema = new Schema(
  {
    name: String,
    slug: String,
    status: String,
    username: String,
    token: String,
    areas_permissions: [
      {
        area: String,
      },
    ],
    group: String,
  },
  {
    collection: INTEGRATION_COLLECTION,
  },
);

export interface Integration {
  name: string;
  slug: string;
  status: string;
  username: string;
  token: string;
  areas_permissions: {
    area: string;
  }[];
  group: string;
}
