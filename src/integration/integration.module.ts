import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { IntegrationSchema } from './integration.model';
import { IntegrationService } from './integration.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

const INTEGRATION_SCHEMA_NAME = 'Integration';

@Module({
  imports: [
    ConfigModule,
    MongooseModule.forFeature([
      {
        name: INTEGRATION_SCHEMA_NAME,
        schema: IntegrationSchema,
      },
    ]),
  ],
  providers: [IntegrationService, ConfigService],
})
export class IntegrationModule {}
