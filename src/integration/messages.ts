export enum Messages {
  NOT_AUTORIZED = 'NOT_AUTORIZED',
  // LOG ERRORS
  LOG_ERROR_FIND_INTEGRATION_API = 'Error on integration api (find integration by username, password and active status)',
  LOG_ERROR_NOT_FOUND_INTEGRATION_API = 'Integration document not found',
  // LOG SUCCESS
  LOG_SUCCESS_INTEGRATION_FOUND = '✅ Integration found successfully',
}
