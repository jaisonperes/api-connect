import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Integration } from './integration.model';
import { Messages } from './messages';

const BASIC_AUTH_ENCODE = 'base64';
const ACTIVE_STATUS = 'active';

@Injectable()
export class IntegrationService {
  private readonly logger = new Logger(IntegrationService.name);
  constructor(
    @InjectModel('Integration')
    private readonly integrationModel: Model<Integration>,
  ) {}

  private basicAuth(auth) {
    const authentication = Buffer.from(
      auth.split(' ')[1],
      BASIC_AUTH_ENCODE,
    ).toString();
    const [username, password] = authentication.split(':');
    return { username, password };
  }

  async authIntegration({ auth }: any): Promise<Integration> {
    const { username, password } = this.basicAuth(auth);
    return this.integrationModel
      .findOne({
        username,
        status: ACTIVE_STATUS,
        token: password,
      })
      .then((integration) => {
        if (!integration) {
          this.logger.error(Messages.LOG_ERROR_NOT_FOUND_INTEGRATION_API);
          throw new UnauthorizedException(Messages.NOT_AUTORIZED);
        }
        this.logger.debug(Messages.LOG_SUCCESS_INTEGRATION_FOUND);
        return integration;
      })
      .catch((err) => {
        this.logger.error(Messages.LOG_ERROR_FIND_INTEGRATION_API, err);
        throw new UnauthorizedException(Messages.NOT_AUTORIZED);
      });
  }
}
