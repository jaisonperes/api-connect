import * as Joi from '@hapi/joi';

export const configValidationSchema = Joi.object({
  PORT: Joi.number().default(3000),
  STAGE: Joi.string().default(''),
  LOG_LEVEL: Joi.string().valid('debug', 'error').default('error'),
  JWT_KEY_FILE: Joi.string().required(),
  JWT_PUBLIC_FILE: Joi.string().required(),
  JWT_ISSUER: Joi.string().required(),
  JWT_EXPIRES: Joi.string().default('3h'),
  MONGO_HOST: Joi.string().required(),
  REGISTER_API: Joi.string().required(),
  VOUCHER_API: Joi.string().required(),
  PLAN_API: Joi.string().required(),
  USER_API: Joi.string().required(),
  MONGO_USER: Joi.string().default('admin'),
  MONGO_PASS: Joi.string().default('admin'),
  DISCORD_WEBHOOK: Joi.string().default(''),
});
