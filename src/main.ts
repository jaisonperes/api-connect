import { LogLevel, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { UserModule } from './user/user.module';
import { Logger } from '@nestjs/common';

const logger = new Logger('bootstrap');

type LogLevelList = LogLevel[];

const SWAGGER_TITLE = 'Api Connect';
const SWAGGER_DESCRIPTION = [
  '### Api para controle de usuários',
  '<p>Esta api foi desenvolvida pela Mais Questões para permitir o controle de usuários por meio de integração.</p>',
  '<p>Baixe o [arquivo de configuração](/api-json) ',
  'para o seu cliente em formato json.</p>',
].join('');
const SWAGGER_VERSION = '1.0';
const SWAGGER_PATH = 'api';

async function bootstrap() {
  const LOG_LEVEL: LogLevelList =
    process.env.LOG_LEVEL === 'debug' ? ['debug', 'log'] : ['error', 'warn'];

  const app = await NestFactory.create(AppModule, {
    logger: LOG_LEVEL,
  });

  const config = new DocumentBuilder()
    .setTitle(SWAGGER_TITLE)
    .setDescription(SWAGGER_DESCRIPTION)
    .setVersion(SWAGGER_VERSION)
    .addBasicAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config, {
    include: [UserModule],
  });

  SwaggerModule.setup(SWAGGER_PATH, app, document);

  app.enableCors();

  app.useGlobalPipes(new ValidationPipe());

  const port = process.env.PORT || 3000;
  await app.listen(port);
  logger.warn(`SERVER INICIALIZED ON PORT "${port}"`);
  logger.warn(`WITH "${LOG_LEVEL.join('" and "')}" LOG LEVELS`);
}
bootstrap();
