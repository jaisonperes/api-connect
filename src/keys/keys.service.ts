import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as fs from 'fs';
import { Messages } from './messages';

const BASIC_AUTH_ENCODE = 'base64';
const PUBLIC_KEY_ENCODE = 'utf-8';
@Injectable()
export class KeysService {
  private readonly logger = new Logger(KeysService.name);
  constructor(private readonly configService: ConfigService) {}

  private readonly BASIC_AUTH_USERNAME = this.configService.get('MONGO_USER');
  private readonly BASIC_AUTH_PASSWORD = this.configService.get('MONGO_PASS');

  private checkAuth(auth): boolean {
    return (
      this.basicAuth(auth).username === this.BASIC_AUTH_USERNAME &&
      this.basicAuth(auth).password === this.BASIC_AUTH_PASSWORD
    );
  }

  private basicAuth(auth) {
    const authentication = Buffer.from(
      auth.split(' ')[1],
      BASIC_AUTH_ENCODE,
    ).toString();
    const [username, password] = authentication.split(':');
    return { username, password };
  }

  getJsonKeys(auth): any {
    if (!auth || !this.checkAuth(auth)) {
      this.logger.error(Messages.LOG_ERROR_BASIC_AUTH_MISSING);
      throw new UnauthorizedException(Messages.NOT_AUTORIZED);
    }
    const PUBLIC_FILE = this.configService.get<string>('JWT_PUBLIC_FILE');
    const publickey = fs.readFileSync(PUBLIC_FILE, PUBLIC_KEY_ENCODE);
    return {
      keys: [publickey],
    };
  }
}
