import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { KeysController } from './keys.controller';
import { KeysService } from './keys.service';

@Module({
  imports: [ConfigModule],
  controllers: [KeysController],
  providers: [KeysService, ConfigService],
})
export class KeysModule {}
