import { Controller, Get, Request } from '@nestjs/common';
import { ApiBasicAuth, ApiResponse } from '@nestjs/swagger';
import { KeysService } from './keys.service';

@Controller('keys')
export class KeysController {
  constructor(private readonly keysService: KeysService) {}

  @Get()
  @ApiResponse({
    status: 200,
    description: 'Keys for authentication',
  })
  @ApiBasicAuth()
  getJsonKeys(@Request() req): any {
    const { authorization } = req.headers;
    return this.keysService.getJsonKeys(authorization);
  }
}
