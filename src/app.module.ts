import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { configValidationSchema } from './config.schema';
import { IntegrationModule } from './integration/integration.module';
import { KeysModule } from './keys/keys.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      ignoreEnvFile: process.env.STAGE != 'dev',
      envFilePath: ['.env.dev'],
      validationSchema: configValidationSchema,
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get('MONGO_HOST'),
        useNewUrlParser: true,
      }),
      inject: [ConfigService],
    }),
    IntegrationModule,
    KeysModule,
    UserModule,
  ],
})
export class AppModule {}
